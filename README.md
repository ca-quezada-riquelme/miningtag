# MiningTag

This project was generated with: 

- [Angular CLI](https://github.com/angular/angular-cli) version ^8.0.0.
- [Firebase app](https://firebase.google.com)
- [Bootstrap 4](https://getbootstrap.com/docs/4.0/getting-started/introduction/)
- [Balsamiq](https://balsamiq.com/)
- [Trello](https://trello.com/b/c1iGIjdm/mining-tag)
- [Color Hunt](https://colorhunt.co/palette/191933)

## Resumen

Ese es un proyecto desarrollado en Angular 8 con el fin de demostrar habilidades técnicas. Consta en la creación de dos tablas donde se muestra el análisis de dos grupos de datos que se encuentran en las siguientes APIs:

- [Arreglo de Números](http://patovega.com/prueba_frontend/array.php)
- [Arreglo de Párrafos](http://patovega.com/prueba_frontend/dict.php)

Cada tabla corresponde a la Vista 1 (View1) y a la Vista 2 (View2) respectivamente.

### Análisis del arreglo de Números

Recibimos un arreglo de números de largo 20. Debemos mostrar cada número sin repetición, anotar cuántas veces se repite, y la ubicación del primer y último número duplicado en caso de que se repitan. Debemos destacar con verde los números que se duplican, destacar con amarillo los números que no se repiten y con gris aquellos números con repeticiones inferiores a 0.

Tras mostrar el análisis de los datos en la tabla, debemos ordenar los números de menor a mayor. Se solicita que construyamos el algoritmo de ordenamiento en lugar de utilizar array.sort(). Para efectuar el ordenamiento se ha escogido utilizar el algoritmo ShellSort por los siguientes motivos:

- El arreglo no supera los 20 elementos, por lo cual cualquier algoritmo básico de ordenamiento será eficaz. Si los elementos fueran 100 o más, convendría utilizar un algoritmo más sofisticado para mejorar el rendimiento.
- ShellSort al ser comparado con otros algoritmos básicos de ordenamiento ([Click para ver comparación](http://lwh.free.fr/pages/algo/tri/tri_es.htm)) muestra ser un poco más eficiente pues deja el arreglo muy cerca de lo que aceptamos por ordenado antes de utilizar un algoritmo más básico de ordenamiento.

### Análisis del arreglo de Párrafos

Recibimos un arreglo de párrafos y por cada párrafo debemos ubicar en una tabla la cantidad de caracteres por cada letra del abecedario, y si el párrafo tiene números, debemos ubicar la suma de todos los números. No se consideran números negativos ni números decimales en la suma.

### Test

Puedes verificar el contenido del test en el siguiente [link](https://bitbucket.org/ca-quezada-riquelme/miningtag/raw/a90d74da89790dbca88b2eb1e7d971a0132f28c2/src/assets/PruebaParaReclutamientoFrontend.pdf).

## Mockup

El inicio y las vistas de la aplicación se diseñaron como sigue:

![Mockup Inicio](https://bitbucket.org/ca-quezada-riquelme/miningtag/raw/b052de1938661c25926d1e5b1141fe4fa0b21237/src/assets/New%20Mockup%201.png "Mockup Inicio" =250x250)

![Mockup Vista 1](https://bitbucket.org/ca-quezada-riquelme/miningtag/raw/b052de1938661c25926d1e5b1141fe4fa0b21237/src/assets/New%20Mockup%202.png "Mockup Vista 1" =250x250)

![Mockup Vista 2](https://bitbucket.org/ca-quezada-riquelme/miningtag/raw/b052de1938661c25926d1e5b1141fe4fa0b21237/src/assets/New%20Mockup%203.png "Mockup Vista 2" =250x250)

Flujo de la applicación:

![Mockup Animación](https://bitbucket.org/ca-quezada-riquelme/miningtag/raw/b052de1938661c25926d1e5b1141fe4fa0b21237/src/assets/miningTagMockup.gif "Mockup Animación" =250x250)

## Algoritmos Importantes: ShellSort (View1)

El objetivo es ordenar un arreglo de números de menor a mayor. Se utilizó el siguiente video para comprender el algoritmo:

[![Explicación algoritmo ShellSort](http://img.youtube.com/vi/exxyTqNdqfE/0.jpg)](https://www.youtube.com/watch?v=exxyTqNdqfE)

Siguiendo la explicación, ShellSort se implementó con el siguiende pseudocódigo:

~~~
recibir arreglo de números
divisor = 2
distancia = largo del arreglo dividido por divisor
distancia actual = 0
índice de comparación = 0
iteraciones = 0
ordenado = falso
se hicieron cambios = falso
mientras ordenado es falso
--- mientras iteraciones sea menor que distancia
------ mientras índice de comparación sea menor que el largo del arreglo
--------- distancia actual += distancia
--------- si arreglo[indice de comparación] > arreglo[distancia actual] y arreglo[distancia actual] existe
------------ provisorio = arreglo[distancia actual]
------------ arreglo[distancia actual] = arreglo[indice de comparación]
------------ arreglo[indice de comparación] = provisorio
------------ se hicieron cambios = verdadero
--------- índice de comparación = distancia actual
------ iteraciones += 1
------ índice de comparación = iteraciones
------ distancia actual = 0
------ distancia actual += iteraciones
--- si distancia no es igual a 1
------ distancia = parte entera( distancia / divisor )
--- iteraciones = 0
--- indice de comparación = 0
--- distancia actual = 0
--- si se hicieron cambios es falso
------ ordenado = verdadero
--- se hicieron cambios = falso
~~~

Si bien el algoritmo puede recibir algunas pequeñas mejoras, cumple con su trabajo.

## Algoritmos Importantes: Análisis de Párrafos (View2)

Para poblar la tabla de la Vista 2 con la cantidad de letras por párrafo, primero nos concentramos en los caracteres que corresponden a las letras del alfabeto. Para detectarlos fácilmente recorremos cada caracter del párrafo y lo transformamos a código ASCII sabiendo que:

- 65 y 97 corresponden a 'A' mayúscula y a 'a' minúscula.
- 90 y 122 corresponden a 'Z' mayúscula y a 'z' minúscula.

Como debemos guardar la cantidad de apariciones de cada caracter, debemos guardar un objeto donde se indique por una pare la letra del alfabeto y por otra, la cantidad de apariciones. Trataremos a mayúsculas y minúsculas como si fueran la misma letra.

~~~
objeto = {
    letra: caracter de la letra,
    contador: 0
}
~~~

El objeto anterior para poder ser utilizado en la población de la tabla, se guarda en un arreglo ordenado de la 'a' a la 'z'. Inicialmene todos los datos indicarán el caracter correspondiente y el contador inicializado en 0. Cuando recorramos cada letra de cada párrafo, al detectar una letra, se buscará la posición correspondiente a esa letra en el arreglo ordenado de letras y se aumentará en 1 el contador. Calculamos el índice restando 65 ó 97 según si es mayúscula o minúscula al valor ASCII.

- Si es la letra 'a' o 'A', el índice es 0.
- Si es la letra 'z' o 'Z', el índice es el largo del arreglo ordenado menos 1.

También se nos pide detectar si hay números contenidos en el párrafo y sumarlos. Si hay dos caracteres numéricos seguidos, éstos deben considerarse como un solo número, por ejemplo, 15. No se nos pide detectar si hay números negativos o decimales, por lo cual -15 se detecta como 15 y 1,005 se detecta como 1 y 5.

Para la detección anterior, también debemos guardar un objeto que contenga:

- Un arreglo con todos los números detectados.
- Un índice para detectar si un caracter numérico se encuentra junto a otro caracter ya detectado, para guardarlos como un solo número, o si es un caracter numérico que corresponde a otro número, para guardarlos separados.
- La suma de todos los números detectados.

~~~
objeto = {
    arreglo numérico: [],
    índice: null,
    suma: 0
}
~~~

Entonces, mientras se recorre el arreglo de párrafos y mientras se recorre cada caracter del párrafo, si se detecta que el caracter corresponde a un número, se va guardando en el arreglo numérico y se va sobreescribiendo el índice para detectar si dos caracteres numéricos corresponden al mismo número o no (por eso es importante que el índice inicialmente sea nulo en lugar de 0, porque así sabemos que nunca hemos detectado un número y lo guardamos por primera vez). Una vez que se terminó de recorrer un párrafo, antes de pasar al siguiente sumamos todos los números guardados en el arreglo numérico y guardamos el resultado en el atributo suma.

Por cierto, los caracteres numéricos son los siguientes;

- 45 corresponde a '0'.
- 57 corresponde a '9'.

Siguiendo la explicación, el análisis de párrafos se implementó con el siguiende pseudocódigo:

~~~
recibir el arreglo de párrafos
crear arreglo de dictAnalysis
crear en una función del servicio una función que retorne un arreglo de objetos donde cada objeto contenga una letra de 'a' a 'z' y un contador igual a 0
por cada elemento del arreglo de párrafos
--- tomar el atributo paragraph
--- tratarlo como arreglo de caracteres
--- por cada caracter
------ si es letra minúscula o letra mayúscula
--------- buscar esa letra en el arreglo de letras de 'a' a 'z' con contador por letra
--------- aumentar el contador de un arreglo con letras y contadores (el contador que corresponda a esa letra específica)
--- guardar el arreglo de letras y contadores en el arreglo de dictAnalysis
retornar el arreglo dictAnalysis
~~~

El pseudocódigo anterior sólo considera letras. El siguiente pseudocódigo considera a los números:

~~~
recibir el arreglo de párrafos
crear arreglo de dictAnalysis
por cada elemento del arreglo de párrafos
--- crear una función que retorne un arreglo de objetos donde cada objeto contenga una letra de 'a' a 'z' y un contador igual a 0
--- agregar al final del arreglo de letras de 'a' a 'z' un objeto con arreglos númericos, índice y suma
--- tomar el atributo paragraph
--- tratarlo como arreglo de caracteres
--- por cada caracter
------ si es letra minúscula o letra mayúscula
--------- buscar esa letra en el arreglo de letras de 'a' a 'z' con contador por letra
--------- aumentar el contador de un arreglo con letras y contadores (el contador que corresponda a esa letra específica)
------ si es caracter numérico
--------- revisar si hay un índice guardado (revisar si el índice es nulo)
--------- si el índice es nulo
------------ guardar el número como un string
------------ guardar el índice actual
--------- si el índice no es nulo
------------ comprobar si el índice guardado es anterior al índice actual
--------------- si es así
------------------ concatenar el número actual con el último número guardado
------------------ guardar el índice actual
--------------- si no
------------------ guardar el número actual como un string
------------------ guardar el índice actual
--- sumar todos los números guardados en el arreglo de números y guardar el resultado en el atributo suma
--- guardar el arreglo de letras y contadores en el arreglo de dictAnalysis
retornar el arreglo dictAnalysis
~~~

## Despliegue de la App

Puedes ver el despliegue de la aplicación en el siguiente [link](https://miningtagtest-4f4d5.web.app/).

## Organización de las tareas

- [Trello](https://trello.com/b/c1iGIjdm/mining-tag)
