import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { DataService } from '../../../app/services/data.service';
import { ProcessingService } from '../../../app/services/processing.service';

@Component({
  selector: 'app-view1',
  templateUrl: './view1.component.html',
  styleUrls: ['./view1.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class View1Component implements OnInit {
  arrData = []; // data after analysis
  arrNumbers = []; // here we store the data's array and then we sorted it
  textValue = '';
  weGetData = false;

  constructor(
    private dService: DataService,
    private pService: ProcessingService
  ) { }

  ngOnInit() {
  }

  getData() {
    if (!this.weGetData) { // if we click twice, we call data only in the first click
      this.dService.getArray().subscribe(data => {
        this.dService.arrData = data.data; // save data array to be analyzed
        this.arrNumbers = data.data; // save data array to be sorted
        this.arrData = this.pService.analyzeArray(this.dService.arrData); // get data analysis
        console.log('View1 data: ', data.data);
        this.weGetData = true; // confirm we don't need to get data again
        this.sortAndShowDataInInput(this.weGetData, this.arrNumbers); // begin to sort data
      });
    }
  }

  // it receive numbers array and bool value and it call functions to sort and show the respective numbers
  sortAndShowDataInInput(boolValue, arr) {
    if (boolValue) { // if we get data
      this.sortNumbersData(arr); // we sort array data
      this.setInputValue(arr); // and we transform the sorted data into string to show it in the textbox
      return true;
    }
  }

  sortNumbersData(arr) {
    setTimeout( () => { // after 100 milliseconds
      this.pService.shellSort(arr); // we sort array data with shellSort algorithm
      return true;
    }, 100);
  }

  setInputValue(arr) {
    setTimeout( () => { // after 102 milliseconds
      this.textValue = arr.toString(); // we transform the sorted array in a string
      return true;
    }, 102);
  }

  changeStyles() {
    // View2's button styles
    (document.querySelector('#btn-our2') as HTMLElement).style.paddingTop = '0.1vh';
    (document.querySelector('#btn-our2') as HTMLElement).style.paddingBottom = '0.1vh';
    (document.querySelector('#btn-our2') as HTMLElement).style.paddingLeft = '8vw';
    (document.querySelector('#btn-our2') as HTMLElement).style.paddingRight = '8vw';
    (document.querySelector('#btn-our2') as HTMLElement).style.display = 'inline-block';
    (document.querySelector('#btn-our2') as HTMLElement).style.marginLeft = 'unset';
    (document.querySelector('#btn-our2') as HTMLElement).style.marginRight = 'unset';

    // View1's button styles
    (document.querySelector('#btn-our1') as HTMLElement).style.paddingTop = '0.1vh';
    (document.querySelector('#btn-our1') as HTMLElement).style.paddingBottom = '0.1vh';
    (document.querySelector('#btn-our1') as HTMLElement).style.paddingLeft = '8vw';
    (document.querySelector('#btn-our1') as HTMLElement).style.paddingRight = '8vw';
    (document.querySelector('#btn-our1') as HTMLElement).style.display = 'inline-block';
    (document.querySelector('#btn-our1') as HTMLElement).style.marginLeft = 'unset';
    (document.querySelector('#btn-our1') as HTMLElement).style.marginRight = 'unset';

    // Show View1's table and hide View2's table
    (document.querySelector('#table-our1') as HTMLElement).style.display = 'block';
    (document.querySelector('#table-our2') as HTMLElement).style.display = 'none';

    // Adjust the app size to the size of View1's absolute table
    (document.querySelector('#trick') as HTMLElement).style.display = 'block';
    (document.querySelector('#trick') as HTMLElement).style.height = '110vh';
  }

}
