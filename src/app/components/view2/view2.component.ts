import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { DataService } from '../../../app/services/data.service';
import { ProcessingService } from '../../../app/services/processing.service';

@Component({
  selector: 'app-view2',
  templateUrl: './view2.component.html',
  styleUrls: ['./view2.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class View2Component implements OnInit {
  arrAToZ = []; // this array is used to generate columns from a to z
  arrDict = []; // dict after analysis
  weGetDict = false;

  constructor(
    private dService: DataService,
    private pService: ProcessingService
  ) { }

  ngOnInit() {
    this.aToZGenerate(); // we need this immediately to generate the header's table
  }

  getDict() {
    if (!this.weGetDict) { // if we click twice, we call data only in the first click
      this.dService.getDict().subscribe(data => {
        console.log(JSON.parse(data.data));
        // We transform the data in javascript object and then we process it
        this.arrDict = this.pService.analyzeDict(JSON.parse(data.data));
        this.weGetDict = true; // confirm we don't need to get data again
      });
    }
  }

  aToZGenerate() { // generate characters from 'a' to 'z' and push them into an array
    const charA = 97; // 97 is 'a' and 122 is 'z'
    const charZ = 122;
    for (let i = charA; i <= charZ; i++) {
      const abcChar = String.fromCharCode(i); // transform code number into characters
      this.arrAToZ.push(abcChar); // generate an array with character from 'a' to 'z'
    }
  }

  changeStyles() {
    // View2's button styles
    (document.querySelector('#btn-our2') as HTMLElement).style.paddingTop = '0.1vh';
    (document.querySelector('#btn-our2') as HTMLElement).style.paddingBottom = '0.1vh';
    (document.querySelector('#btn-our2') as HTMLElement).style.paddingLeft = '8vw';
    (document.querySelector('#btn-our2') as HTMLElement).style.paddingRight = '8vw';
    (document.querySelector('#btn-our2') as HTMLElement).style.display = 'inline-block';
    (document.querySelector('#btn-our2') as HTMLElement).style.marginLeft = 'unset';
    (document.querySelector('#btn-our2') as HTMLElement).style.marginRight = 'unset';

    // View1's button styles
    (document.querySelector('#btn-our1') as HTMLElement).style.paddingTop = '0.1vh';
    (document.querySelector('#btn-our1') as HTMLElement).style.paddingBottom = '0.1vh';
    (document.querySelector('#btn-our1') as HTMLElement).style.paddingLeft = '8vw';
    (document.querySelector('#btn-our1') as HTMLElement).style.paddingRight = '8vw';
    (document.querySelector('#btn-our1') as HTMLElement).style.display = 'inline-block';
    (document.querySelector('#btn-our1') as HTMLElement).style.marginLeft = 'unset';
    (document.querySelector('#btn-our1') as HTMLElement).style.marginRight = 'unset';

    // Show View2's table and hide View1's table
    (document.querySelector('#table-our2') as HTMLElement).style.display = 'block';
    (document.querySelector('#table-our1') as HTMLElement).style.display = 'none';

    // Adjust the app size to the size of View2's absolute table
    (document.querySelector('#trick') as HTMLElement).style.display = 'block';
    (document.querySelector('#trick') as HTMLElement).style.height = '125vh';
  }

}
