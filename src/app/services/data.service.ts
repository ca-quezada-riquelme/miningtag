import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataService { // Get data from web APIs
  arrData = [];

  constructor(
    private http: HttpClient
  ) { }

  getArray(): Observable<any> {
    return this.http.get('https://patovega.com/prueba_frontend/array.php');
  }

  getDict(): Observable<any> {
    return this.http.get('https://patovega.com/prueba_frontend/dict.php');
  }

}
