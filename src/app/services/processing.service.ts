import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ProcessingService {
  arrDataAnalysis = []; // we use this array to complete view1's table
  arrDictAnalysis = []; // we use this array to complete view2's table
  charAminus = 97; // 97 is 'a' and 122 is 'z'
  charZminus = 122;
  charAMayus = 65; // 65 is 'A' and 90 is 'Z'
  charZMayus = 90;
  char0 = 48; // 48 is '0' abd 57 is '9'
  char9 = 57;

  constructor() { }

  createAnalyzeData(dataNumber: number, count: number, fstIndex: number, lstIndex: number) {
    const object = {
      data: dataNumber,
      quantity: count,
      firstPosition: fstIndex,
      lastPosition: lstIndex
    };
    return object;
  }

  analyzeArray(arrData) {
    const arrLength = arrData.length;
    for (let i = 0; i < arrLength; i++) {
      if (this.arrDataAnalysis.length === 0) { // if array dataAnalysis is empty
        // we store number, only one repetition of that number and position (first index and last index are the same)
        this.arrDataAnalysis.push(
          this.createAnalyzeData(arrData[i], 1, i, i)
          );
      } else {
        // if array dataAnalysis isn't empty, we have to look if the number is already stored
        const arrDALength = this.arrDataAnalysis.length;
        let dataIsStored = false;
        for (let j = 0; j < arrDALength; j++) {
          // if the number is already stored
          if (arrData[i] === this.arrDataAnalysis[j].data) {
            this.arrDataAnalysis[j].quantity += 1; // increment quantity
            this.arrDataAnalysis[j].lastPosition = i; // update last position
            dataIsStored = true; // now we stored the data
            break; // we don't expect another copy inside this array
          }
        }
        // if the number isn't stored
        if (!dataIsStored) {
          // we store number, only one repetition of that number and position (first index and last index are the same)
          this.arrDataAnalysis.push(
            this.createAnalyzeData(arrData[i], 1, i, i)
            );
        }
      }
    }
    return this.arrDataAnalysis;
  }

  returnArrAtoZ() { // return an array of objects with characters from ´a´ to ´z´ and a counter where we store the amount of each character
    const arrAtoZ = [];
    for (let i = this.charAminus; i <= this.charZminus; i++) { // from ´a´ to ´z´
      const abcChar = String.fromCharCode(i); // we store the string of charCode
      const abcObject = {
        alphabet: abcChar,
        count: 0 // here we store the amount of the character abcChar
      };
      arrAtoZ.push(abcObject);
    }
    return arrAtoZ;
  }

  fromCodeCharToIndex(codeChar: number) {
    // if the codeChar correspond to an alphabet's character, we return an index appropriate to arrDictAnalysis array
    let index = 0;
    if ((codeChar >= this.charAminus) && (codeChar <= this.charZminus)) { // if codeChar is minus
      index = codeChar - this.charAminus;
    } else if ((codeChar >= this.charAMayus) && (codeChar <= this.charZMayus)) { // if codeChar is Mayus
      index = codeChar - this.charAMayus;
    } else { // if codeChar is not an alphabet's character
      console.error('fromCodeCharToIndex() está recibiendo un número que no corresponde a los caracteres dentro del alfabeto.');
    }
    return index;
  }

  addNumberObject() { // create an object with an empty array numbers and null index
    const numberObject = {
      arrNumbers: [],
      index: null,
      sum: 0
    };
    return numberObject;
  }

  getSum(numberObject) { // receive numberObject and return sum according to numberObject.arrNumbers
    if (numberObject.index === null) { // if the index is null, we don't have numbers to process.
      return 0;
    } else { // we store the sum of all numbers in result variable
      let result = 0;
      numberObject.arrNumbers.forEach(element => {
        result += parseInt(element, 10);
      });
      return result;
    }
  }

  analyzeDict(arrDict) {
    const arrLength = arrDict.length;
    for (let i = 0; i < arrLength; i++) { // for each paragraph in Dict array
      const arrAtoZ = this.returnArrAtoZ(); // we need one of this array for each paragraph analysis
      arrAtoZ.push(this.addNumberObject()); // we need to add an object to manage number characters
      const paragraph = arrDict[i].paragraph;
      const stringLength = paragraph.length;
      for (let j = 0; j < stringLength; j++) { // for each character in paragraph
        const codeChar = paragraph.charCodeAt(j);
        // if the character correspond to the alphabet (minus or mayus)
        if (((codeChar >= this.charAminus) && (codeChar <= this.charZminus)) ||
            ((codeChar >= this.charAMayus) && (codeChar <= this.charZMayus))) {
              // find that character in arrAtoZ array
              const index = this.fromCodeCharToIndex(codeChar);
              const arrAtoZObject = arrAtoZ[index];
              // increment their counter
              arrAtoZObject.count += 1;

        // if the character isn't an alphabet's character, check if is a number
        } else if ((codeChar >= this.char0) && (codeChar <= this.char9)) {
          // check if a number's index was stored
          if (arrAtoZ[arrAtoZ.length - 1].index === null) {
            // if the number's index wasn't stored, we store current number as a string and current index
            arrAtoZ[arrAtoZ.length - 1].index = j;
            arrAtoZ[arrAtoZ.length - 1].arrNumbers.push(String.fromCharCode(codeChar));
          } else {
            // if the number's index was stored, we have to check if the current index is the stored index plus 1
            if ((arrAtoZ[arrAtoZ.length - 1].index + 1) === j) {
              // if the condition is true, we concat the current number with the last number in the numbers array...
              arrAtoZ[arrAtoZ.length - 1].arrNumbers[arrAtoZ[arrAtoZ.length - 1].arrNumbers.length - 1] =
              arrAtoZ[arrAtoZ.length - 1].arrNumbers[arrAtoZ[arrAtoZ.length - 1].arrNumbers.length - 1]
              .concat(String.fromCharCode(codeChar));
              // ... and store current index
              arrAtoZ[arrAtoZ.length - 1].index = j;
            } else {
              // if the condition is false, we store current index and current number without concat
              // Note for the developer:
              // ( this condition can be implemented inside this line : "" if (arrAtoZ[arrAtoZ.length - 1].index === null) { "" )
              arrAtoZ[arrAtoZ.length - 1].index = j;
              arrAtoZ[arrAtoZ.length - 1].arrNumbers.push(String.fromCharCode(codeChar));
            }
          }
        }
      }
      // sum all numbers detected inside the paragraph
      arrAtoZ[arrAtoZ.length - 1].sum = this.getSum(arrAtoZ[arrAtoZ.length - 1]);
      // store the paragraph's analysis into arrDictAnalysis
      this.arrDictAnalysis.push(arrAtoZ);
    }
    console.log('this.arrDictAnalysis: ', this.arrDictAnalysis);
    return this.arrDictAnalysis;
  }

  shellSort(arrNumbers) {
    const divisor = 2;
    let distance = Math.floor(arrNumbers.length / divisor); // we have to choose a max distance
    let currentDistance = 0; // this is the index of an array's element to be compared with the array's element in the comparisonIndex
    let comparisonIndex = 0;
    let iterations = 0;
    let ordered = false;
    let swapWasDone = false;
    while (!ordered) { // while the array isn't ordered
      while (iterations < distance) { // while the iterations over a distance are not completed
        console.log('iterations: ', iterations);
        while (comparisonIndex < (arrNumbers.length - 1)) { // while we have not reached the end of the array
          currentDistance += distance; // we define the array's element to becompared with the array's element in the comparisonIndex
          console.log('¿ arrNumbers[', comparisonIndex, '] > arrNumbers[', currentDistance, '] ?');
          console.log('   o dicho de otro modo');
          console.log('¿ ', arrNumbers[comparisonIndex], ' > ', arrNumbers[ currentDistance], ' ?');
          if ((arrNumbers[comparisonIndex] > arrNumbers[currentDistance]) &&
              (arrNumbers[currentDistance] !== undefined)) {
            console.log('swap');
            // if the element in the comparisonIndex is greater than the element in the currentDistance, we swap both values
            // (we have to verify if currentDistance is an index inside the array.)
            const aux = arrNumbers[currentDistance];
            arrNumbers[currentDistance] = arrNumbers[comparisonIndex];
            arrNumbers[comparisonIndex] = aux;
            console.log('arrNumbers: ', arrNumbers);
            swapWasDone = true;
          }
          // now we have to compare the element in the currentDistance with the element in the currentDistance + distance
          comparisonIndex = currentDistance;
          console.log('comparisonIndex: ', comparisonIndex);
        }
        // when we have done the comparisons of the first element in the array and of all elements in the differents currentDistances,
        // we have to do the same with the second, third, etc. elements and their respective currentDistances
        // until the number of iterations (e.g. 0,1,2,3,4) done is the number of distances (e.g. 5)
        iterations += 1;
        comparisonIndex = iterations;
        currentDistance = 0; // if we don't restart currentDistance, we get indexes greater than array length
        currentDistance += iterations; // we have to maintain the same distance between comparisonIndex and currentDistance in this cycle.
      }
      // when we have done all the correspondent iterations inside one distance value, the distance must be updated,
      // except when distance === 1 (always we first compare the first position, index 0, with the distance)
      if (distance !== 1) {
        distance = Math.floor(distance / divisor);
      }
      // now we have to do the same iterations with the new distance
      iterations = 0;
      comparisonIndex = 0;
      currentDistance = 0;
      if (!swapWasDone) {
        // if we never done a swap, we consider an ordered array
        ordered = true;
      }
      // if the swap was done, we update swapWasDone value
      swapWasDone = false;
    }
    console.log('shellSort array: ', arrNumbers);
    // return arrNumbers;
  }

}
