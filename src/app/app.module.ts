import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

// Services
import { DataService } from './services/data.service';
import { ProcessingService } from './services/processing.service';

// Components
import { AppComponent } from './app.component';
import { View1Component } from './components/view1/view1.component';
import { View2Component } from './components/view2/view2.component';

@NgModule({
  declarations: [
    AppComponent,
    View1Component,
    View2Component
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [
    DataService,
    ProcessingService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
